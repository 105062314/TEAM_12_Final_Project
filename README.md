# Software Studio 2018 Spring TEAM 12 Final Project

* **Gitlab Game URL : https://105062314.gitlab.io/TEAM_12_Final_Project/**

## 遊戲畫面
<img src="game.png" width="700px" height="500px"></img>
<img src="game2.png" width="700px" height="500px"></img>

## Checking with Proposal 
|                                              Item                                              | check |
|:----------------------------------------------------------------------------------------------:|:-----:|
|   滑鼠左鍵按下即可駕駛太空船，可跳至不同載具                       |  yes  |
|   在時限內跳上下一個太空船，否則死亡                              |  yes  |
|   持續前進直至落地為止，創造高分                                 |  yes  |
|   隨機產生新太空船                                             |  yes  |
|   定時自動跳                                                  |  yes  |
|   抓取位置判斷                                                |  yes  |
|   排行榜(firebase)                                           |  yes  |
|   道具系統                                                    |  yes  |
|   不同的角色技能（發射子彈、跳躍距離不同）                         |  yes  |
|             |  yes  |

## About Team 12 Final Project
* Idea from 瘋狂動物園
* 主要功能

    1. Menu : 可選擇進行遊戲及觀看排行榜
    2. Menu小動畫，可操作角色移動左右，選單箭頭移動
    3. 利用滑鼠操作角色位置
    4. 按下空白鍵發射角色，跳往不同載具
    5. 陷阱：山、礦山、天文台、尖刺
    6. 兩側建築物為邊界
    7. 有life跟分數計算功能
    8. 道具：愛心（下一次碰撞不會死亡）、星星（無敵）
    9. 發射子彈：特定飛機有發射子彈功能，可消除障礙物
    10. 判定死亡：非無敵模式或沒有吃到愛心時，碰撞任何物體
    11. 排行榜(firebase)、排行榜動畫
    12. 暫停功能
    13. 音效配置（背景音樂、選單音樂）

* 特殊處

    1. 遊戲進行方向為斜向（約45度角）
    2. 支援滑鼠操作
    3. 根據視窗大小（筆電或手機）來自動設定長寬
    4. loading page 先等待圖面仔入完成後才到menu

## Function Detail
|       **states**      |              handle           |
|:---------------------:|:-----------------------------:|
|       **boot**        |   先載入loadState要用到的東西    |
|       **load**        |   先載入全部圖片，並有進度條      |
|       **menu**        |   判斷進入遊戲或排行榜           |
|       **main**        |   開始遊戲                     |
|       **rank**        |   排行榜                       |

## html 部分
* 加入RWD及利用CSS調整視窗大小

## JavaScript - Phaser 
# Basic
* Spritesheet
    * 由於我們需要的角色動畫較特別，因此自己製作Spritesheet
    * 利用Photoshop將多張人物調整成我們需要的角度，再組合成一張Spritesheet
* 鍵盤、觸控及滑鼠
    * createCursorKeys() 此function能包含上下左右等方向鍵
    * 自訂keyboard（game.input.keyboard.addKeys）可設定所需要之按鍵
    * game.input.mousePointer 滑鼠點擊操作
* 背景、兩側建築移動

# Advance
* 角色拖曳
    * 利用drag
* 角色跳躍
* 本體轉換
* 子彈發射
    * 使用weapon function
    * 再產生飛機時，即根據飛機種類，加入不同weapon，避免武器產生太慢
    * 在create中，設定weapon角度、方向、速度、頻率等等
    * 在update中，跟隨目前飛機位置，並判斷是否fire
* 飛機產生
* 道具
* 障礙物產生
* 碰撞判定
* 音樂
    * game.load.audio載入音樂
    * loop 設定重複播放 volume設定大小聲
    * play() stop()進行播放及停止
* 排行榜
    * 在結束時，將玩家成績上傳至firebase上
    * 利用midterm project學到之技巧，讀出前三高的名次
    * 存入時我利用負數，這樣存取時較為方便

## Reference
* https://phaser.io/examples/v2/input/drag/
* https://phaser.io/examples/v2/input/follow-mouse/
* http://www.html5gamedevs.com/topic/21639-phaser-scale-manager-show-all-not-working/