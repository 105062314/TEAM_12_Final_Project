var loadState = {
    preload:function(){

        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,'loading...', { font: '30px Arial', fill: '#ffffff' }); 
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar'); 
        progressBar.scale.setTo(1.5,0.4);
        progressBar.anchor.setTo(0.5, 0.5); 
        game.load.setPreloadSprite(progressBar);

        // Image : load images
        game.load.spritesheet('player', 'assets/player.png', 45, 67);
        game.load.spritesheet('player_shadow', 'assets/player_shadow.png', 45, 67);
        game.load.image('ground','./assets/ground.png');
        game.load.image('ground1','./assets/ground2.png');
        game.load.image('aircraft0','./assets/aircraft.png');
        game.load.image('aircraft1','./assets/aircraft01.png');
        game.load.image('aircraft2','./assets/aircraft02.png');
        game.load.image('aircraft3','./assets/aircraft03.png');
        game.load.image('aircraft4','./assets/aircraft04.png');
        game.load.image('aircraft5','./assets/aircraft05.png');
        
        game.load.image('mountain1','./assets/rocks_NE.png');
        game.load.image('mountain2','./assets/rocks_NW.png');
        game.load.image('mountain3','./assets/rocks_SE.png');
        game.load.image('mountain4','./assets/rocksOre_SW.png');
        game.load.image('mountain5','./assets/stationLarge_SE.png');
        game.load.image('left_building','./assets/left_building.png');
        game.load.image('right_building','./assets/right_building.png');
        game.load.image('laser', 'assets/laser.png');
        game.load.image('bullet1', 'assets/bullet1.png');
        game.load.image('bullet2', 'assets/bullet2.png');
        game.load.image('heart','./assets/heart.png');
        game.load.image('star','./assets/star.png');
        game.load.image('play1', './assets/play1.png');

        game.load.image('pixel', './assets/pixel.png');

        game.load.image('menu_floor', './assets/menu_floor.png');
        game.load.spritesheet('cyberman', './assets/cyberman.png', 31.5, 48);
        game.load.image('bubble1', './assets/bubble1.png');
        game.load.image('bubble2', './assets/bubble2.png');
        game.load.image('bubble3', './assets/bubble3.png');
        game.load.image('tri', './assets/light.png');
        game.load.image('trap', './assets/trap.png');
        // Sound: 
        game.load.audio('mainMusic', 'assets/sounds/bgm.wav');
        game.load.audio('menuMusic', 'assets/sounds/menu_music.wav');
        game.load.audio('gameOver', 'assets/sounds/gameover.wav');
        game.load.audio('boom', 'assets/sounds/boom.wav');

        // Setting game window size
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    },
    create:function(){

    },
    update:function(){
        game.state.start('menu');
    }
};