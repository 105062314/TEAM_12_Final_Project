var hitcount = 0;

var mainState = {
    preload:function(){},
    create:function(){

        game.stage.backgroundColor = '#227700';
        game.renderer.renderSession.roundPixels = true; // Setup renderer
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.cursor = game.input.keyboard.createCursorKeys();

        this.bgm = game.add.audio('mainMusic');
        this.bgm.loop = true;
        this.bgm.play();
        this.bgm.volumn = 0.6;

        this.boomSound = game.add.audio('boom');

        //creta bg
        this.createbg();
        this.createleft();
        this.createright();
        this.createplayer();

        this.createTextBoard();

        
        

        this.initCrafts();

        this.lastTime = game.time.now;
        this.jumpTime = game.time.now;
        this.aboardTime = game.time.now;
        this.mountainTime = game.time.now;
        this.itemTime = game.time.now;
        this.pauseTime = game.time.now;
        this.crashTime = game.time.now;
        this.hitTime = game.time.now;

        this.pause1 = false;

        game.global.recent_score = 0;

        this.mountain = [];
        this.item = [];
        this.itemkind = [];
        this.life = false;
        this.hit = true;

        this.keyboard2 = game.input.keyboard.addKeys(
            {
                'enter': Phaser.Keyboard.ENTER,
                'p':Phaser.Keyboard.P,
        });

        //pause
        this.play1 = game.add.sprite(400, 250, 'play1');
        this.play1.anchor.set(0.5, 0.5);
        this.play1.scale.setTo(0.2,0.2);
        this.play1.visible = false;
    
        //emitter
        
        this.emitter = game.add.emitter(0, 0, 30);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(3, 0, 3, 0, 2000);
        this.emitter.gravity = 0;

        // player jump
        this.fire = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.fire.onDown.add(this.jump, this);

        //create texts
        var style = {fill: 'white', fontSize: '20px'}
        //this.life = game.add.text(500, 100, '', style);
        this.score = game.add.text(500, 200, '', style);
        this.restart_enter = game.add.text(110, 100, 'Press Enter to restart', style);
        this.final_score= game.add.text(140, 140, '', style);
        this.restart_enter.visible = false;
        this.final_score.visible = false;  

        //Sounds

        //pointer
        this.point = game.input.addPointer();

        //last_shaft_time = game.time.now;
        
        if(game.global.player_name == undefined) {
            game.state.start('menu');
        }            
    },
    
    update:function(){
        var keyboard2 = game.input.keyboard.addKeys({'enter': Phaser.Keyboard.ENTER});

        game.global.recent_score += 0.1; 

        //game.physics.arcade.collide(this.player, [this.left,this.right]);
        this.aircraft.bringToTop();
      

        if(this.pause1 == true){
            this.lastTime = game.time.now;
            this.jumpTime = game.time.now;
            this.aboardTime = game.time.now;
            this.mountainTime = game.time.now;
            this.aircraft.inputEnabled = false;
            this.play1.visible = true;
        }
        else {
            // this.lastTime = this.lastTime;
            // this.jumpTime = this.jumpTime;
            // this.aboardTime = this.aboardTime;
            // this.mountainTime = this.mountainTime;
            this.updatebg();
            this.updateleft();
            this.updateright();
            this.createCraft();
            this.updateCraft();
            this.createMountain();
            this.updateMountain();
            this.updateweapon();
            this.updateTextBoard();

            this.createItem();
            this.updateItem();
            this.objectControl();
            this.play1.visible = false;
        }

        // console.log(this.player.x, this.player.y);

        if (this.player.visible==false && game.time.now-this.aboardTime > 4000) {
            game.camera.flash(0xF5F5DC, 1000);
            game.camera.shake(0.03, 1000);
        }

        if(this.aircraft.x*(-0.7)-this.aircraft.y>-187 || this.aircraft.x*(-0.7)-this.aircraft.y<-700) {
            if (!this.life && game.time.now-this.crashTime > 500) {
                this.bgm.stop();
                this.boomSound.play();
                game.time.events.add(400, function () {
                    game.state.start('rank');
                }, this);
            }
            else {
                this.life = false;
                this.crashTime = game.time.now;
            }
        }

        
        
        else if (this.player.visible==true && game.time.now-this.jumpTime > this.jumpInterval()) {
            if (!this.life && game.time.now-this.crashTime > 500) {
                this.bgm.stop();
                
                this.player.body.velocity.x = 0;
                this.player.body.velocity.y = 0;
                this.player_shadow.body.velocity.x = 0;
                this.player_shadow.body.velocity.y = 0;
                this.emitter.x = this.player.x; 
                this.emitter.y = this.player.y; 
                this.emitter.start(true, 2000, null, 15);
                game.time.events.add(
                    400,function() {
                        game.state.start('rank');}, this);
                
                
            }
            else {
                this.life = false;
                this.crashTime = game.time.now;
            }
        }
        
        
        else if (this.player.visible==false && game.time.now-this.aboardTime > 6000) {
            
            game.state.start('rank');
            this.bgm.stop();
            
        }
        else if (this.player.position.y < 10) {
            game.state.start('rank');
            this.bgm.stop();
        }

        

        // console.log(this.life);
        if (game.time.now-this.hitTime > 3000) {
            this.hit = true;
        }


        if(this.keyboard2.p.isDown && game.time.now-this.pauseTime > 200){
            this.pause1 = !this.pause1;
            this.pauseTime = game.time.now;
            if(this.pause1)
                this.bgm.pause();
            else
                this.bgm.resume();

        }

        var keyboard3 = game.input.keyboard.addKey(Phaser.Keyboard.B);
        if(keyboard3.isDown){
            game.state.start('rank');
            this.bgm.stop();
        }
        // console.log(this.jumpTime, game.time.now);
            
        
        
        // if(status=="gameover")
        //     this.restart();
    },

    initCrafts: function () {
        this.Crafts = []; 

        //create player
        var aircraft;
        aircraft = game.add.sprite(70, 305, 'aircraft0');
        aircraft.kind = 0;
        aircraft.scale.setTo(0.4,0.4);
        aircraft.anchor.setTo(0.5, 0.5);
        aircraft.angle = 52;
        aircraft.weapon = game.add.weapon(30, 'laser');
        aircraft.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        aircraft.weapon.bulletSpeed = 400;
        aircraft.weapon.fireRate = 50;
        aircraft.weapon.bulletAngleOffset = 0;
        aircraft.weapon.bulletAngleOffset = 90;
        aircraft.weapon.fireAngle = -35;
        game.physics.arcade.enable(aircraft.weapon);

        // aircraft.inputEnabled= true;
        // aircraft.input.enableDrag();
        game.physics.arcade.enable(aircraft);
        this.Crafts.push(aircraft);
        this.craftID = 0;
        this.aircraft = this.Crafts[this.craftID];
        this.createShadow(this.aircraft);
    },

    createbg: function(){
        var startX = -2370;
        var startY = 863;
        var nextX = 225;
        var nextY = -157;

        this.bgtiles = [];

        for(var i=0; i<12; i++)
        {
            var x = startX + nextX*i;
            var y = startY + nextY*i;
            this.bgtiles[i] = game.add.tileSprite(x, y, 1050,2000,'ground1');
            this.bgtiles[i].scale.setTo(2.5, 2.5);
        }
    },
    
    createleft: function(){
        var startX = 170;
        var startY = 5;
        var nextX = 337;
        var nextY = -235;
        this.leftside = [];

        for(var i=4; i>=0; i--)
        {
            var x = startX + nextX*i;
            var y = startY + nextY*i;
            this.leftside[i] = game.add.sprite(x, y,'left_building');
        }
    },

    createright: function(){
        var startX = 750;
        var startY = 200;
        var nextX = 351;
        var nextY = -245;
        this.rightside = [];

        for(var i=4; i>=0; i--)
        {
            var x = startX + nextX*i;
            var y = startY + nextY*i;
            this.rightside[i] = game.add.sprite(x, y,'right_building');
        }
    },
    createplayer: function(){
        this.player = game.add.sprite(0, 300,'player');
        this.player.scale.setTo(0.8,0.8);
        this.player.anchor.setTo(0.5, 0.5);
        this.player.visible = false;

        this.player_shadow = game.add.sprite(0, 340,'player_shadow');
        this.player_shadow.scale.setTo(0.8,0.8);
        this.player_shadow.anchor.setTo(0.5, 0.5);
        this.player_shadow.visible = false;
        game.physics.arcade.enable(this.player);
        game.physics.arcade.enable(this.player_shadow);

        // animations
        this.player.animations.add('fly1', [0,1.2,3,4,5,6], 15,true);
        this.player_shadow.animations.add('fly2', [0,1.2,3,4,5,6], 15,true);
    },

    
    updatebg: function(){
        var startX = this.bgtiles[this.bgtiles.length-1].position.x;
        var startY = this.bgtiles[this.bgtiles.length-1].position.y;
        var nextX = 225;
        var nextY = -157;


        for(var i=0; i<this.bgtiles.length; i++)
        {
            var tile =this.bgtiles[i];
            tile.position.x-=2.25;
            tile.position.y+=1.57;
            tile.sendToBack();

            if(tile.position.x<-2600)
            {
                tile.destroy();
                this.bgtiles.splice(0, 1);
                var x = startX + nextX;
                var y = startY + nextY;
                this.bgtiles[this.bgtiles.length] = game.add.tileSprite(x, y, 1050,2000, 'ground1');
                this.bgtiles[this.bgtiles.length-1].scale.setTo(2.5, 2.5);
            }
        }
    },

    updateleft: function(){
        var startX = this.leftside[this.leftside.length-1].position.x;
        var startY = this.leftside[this.leftside.length-1].position.y;
        var nextX = 337;
        var nextY = -235;


        for(var i=0; i<this.leftside.length; i++)
        {
            var tile =this.leftside[i];
            tile.position.x-=3.37*0.6678;
            tile.position.y+=2.35*0.6678;
            tile.angle = 55;
            // collide
            //game.physics.arcade.collide(this.aircraft, tile, mainState.gameover(), null, this);
            // console.log(status);

            game.physics.arcade.enable(tile);
            tile.body.enable = true;
            game.physics.arcade.collide(this.aircraft, tile);

            if(tile.position.y>500)
            {
                tile.destroy();
                this.leftside.splice(0, 1);
                var x = startX + nextX;
                var y = startY + nextY;
                this.leftside[this.leftside.length] = game.add.sprite(x, y,'left_building');
            }
        }
    },

    updateright: function(){
        var startX = this.rightside[this.rightside.length-1].position.x;
        var startY = this.rightside[this.rightside.length-1].position.y;
        var nextX = 351;
        var nextY = -245;


        for(var i=0; i<this.rightside.length; i++)
        {
            var tile =this.rightside[i];
            tile.position.x-=3.51*0.641;
            tile.position.y+=2.45*0.641;
            tile.angle = 50;
            //game.physics.arcade.collide(this.aircraft, tile, mainState.gameover(), null, this);
            // console.log(status);

            if(tile.position.y>500)
            {
                tile.destroy();
                this.rightside.splice(0, 1);
                var x = startX + nextX;
                var y = startY + nextY;
                this.rightside[this.rightside.length] = game.add.sprite(x, y,'right_building');
            }
        }
    },

    


    createCraft: function () {
        if (game.time.now > this.lastTime + 3600) {
            this.lastTime = game.time.now;
            this.createOneCraft();
        }
    },

    createOneCraft: function () {
        var aircraft;
        var x = Math.random()*360+490;
        var randomNum = Math.random()*500;

        if (randomNum <= 150) {
            aircraft = game.add.sprite(x, -114, 'aircraft0');
            aircraft.kind = 0;
        }
        else if (randomNum <= 200) {
            aircraft = game.add.sprite(x, -114, 'aircraft1');
            aircraft.kind = 1;
            aircraft.weapon = game.add.weapon(30, 'laser');
            aircraft.weapon.fireAngle = -35;
            aircraft.weapon.bulletAngleOffset = 90;
            aircraft.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        }
        else if (randomNum <= 250) {
            aircraft = game.add.sprite(x, -114, 'aircraft2');
            aircraft.kind = 2;
            aircraft.weapon = game.add.weapon(30, 'bullet1');
            aircraft.weapon.fireAngle = -35;
            aircraft.weapon.bulletAngleOffset = 90;
            aircraft.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        }
        else if (randomNum <= 300) {
            aircraft = game.add.sprite(x, -114, 'aircraft3');
            aircraft.kind = 3;
        }
        else if (randomNum <= 350) {
            aircraft = game.add.sprite(x, -114, 'aircraft4');
            aircraft.kind = 4;
            aircraft.weapon = game.add.weapon(30, 'bullet2');
            aircraft.weapon.fireAngle = -35;
            aircraft.weapon.bulletAngleOffset = 90;
            aircraft.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        }
        else {
            aircraft = game.add.sprite(x, -114, 'aircraft5');
            aircraft.kind = 5;
        }

        // aircraft = game.add.sprite(x, -114, 'aircraft');
        aircraft.scale.setTo(0.4, 0.4);
        aircraft.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(aircraft);
        this.createShadow(aircraft);
        this.Crafts.push(aircraft);
        // console.log('create');
    },

    updateCraft: function () {
        for(var i=0; i<this.Crafts.length; i++)
        {
            var tile =this.Crafts[i];
            this.updateShadow(tile);
            if (i != this.craftID) {
                
                tile.position.x-=2.25;
                tile.position.y+=1.57;
                //game.physics.arcade.collide(this.aircraft, tile, mainState.gameover(), null, this);
                tile.angle = 52;

                
        
                // console.log(status);
                // tile.sendToBack();
                if(Phaser.Math.distance(this.aircraft.position.x, this.aircraft.position.y , tile.position.x , tile.position.y)< 90 && this.hit) {
                    if (!this.life && game.time.now-this.crashTime > 500) {
                        this.bgm.stop();
                        this.boomSound.play();
                        game.time.events.add(400, function () {
                            game.state.start('rank');
                        }, this);
                    }
                    else {
                        this.life = false;
                        this.crashTime = game.time.now;
                    }
                }
                    

                if(Phaser.Math.distance(this.player.position.x, this.player.position.y , tile.position.x , tile.position.y) < this.acceptArea())
                {
                    if (this.player.visible == true) {
                        this.Crafts[this.craftID].inputEnabled = false;
                        this.endjump(); 
                        this.craftID = i;
                    }
                }   

                if(tile.position.x<-2600)
                {
                    tile.destroy();
                    this.Crafts.splice(i, 1);
                    if (this.craftID > i) {
                        this.craftID--;
                    }
                }
            }
            
        }
    },

    updateweapon: function(){

        if(this.aircraft.weapon==undefined)
            return;
        this.aircraft.weapon.trackSprite(this.aircraft, 0, 0);
        if(this.aircraft.kind==1||this.aircraft.kind==2||this.aircraft.kind==4)
            this.aircraft.weapon.fire();
    },

    createMountain: function () {
        if (game.time.now > this.mountainTime + 5700 && game.time.now > this.lastTime + 1000) {
            this.mountainTime = game.time.now;
            this.createOneMountain();
            // console.log('shit');
        }
    },

    createOneMountain: function () {
        var obstacle;
        var x = Math.random()*360+540;
        var num = game.rnd.integerInRange(0,5);

        if(num==0){
            obstacle = game.add.sprite(x, -114, 'mountain1');
        }
        else if(num==1){
            obstacle = game.add.sprite(x, -114, 'mountain2');
        }
        else if(num==2){
            obstacle = game.add.sprite(x, -114, 'mountain3');
        }
        else if(num==3){
            obstacle = game.add.sprite(x, -114, 'mountain4');
        }
        else if(num==4){
            obstacle = game.add.sprite(x, -114, 'mountain5');
        }
        else{
            obstacle = game.add.sprite(x, -114, 'trap');
        }
        obstacle.scale.setTo(1.5, 1.5);
        obstacle.anchor.setTo(0.5, 0.5);
        this.mountain.push(obstacle);
        // console.log('create');
    },

    updateMountain: function () {
        for(var i=0; i<this.mountain.length; i++)
        {
            var tile =this.mountain[i];
            
                
            tile.position.x-=2.25;
            tile.position.y+=1.57;
            //game.physics.arcade.collide(this.aircraft, tile, mainState.gameover(), null, this);
            // tile.angle = 52;

            game.physics.arcade.collide(this.weapon, tile, mainState.weapon_hit(tile), null, this);
    
            // console.log(status);
            // tile.sendToBack();
            if(Phaser.Math.distance(this.aircraft.position.x, this.aircraft.position.y , tile.position.x , tile.position.y)< 90 && this.hit) {
                if (!this.life && game.time.now-this.crashTime > 500) {
                    this.bgm.stop();
                    this.boomSound.play();
                    game.time.events.add(400, function () {
                        game.state.start('rank');
                    }, this);
                }
                else {
                    this.life = false;
                    this.crashTime = game.time.now;
                }
                    
            }
                
            if(tile.position.x<-2600)
            {
                tile.destroy();
                this.mountain.splice(i, 1);
            }
        }
            
        
    },

    weapon_hit: function(tile){
        //tile.tint = 0xff0000;
        //game.add.tween(tile).to( { alpha: 0 }, 200, Phaser.Easing.Linear.None, true, 0, 1000, true);
        if(this.aircraft.kind==1||this.aircraft.kind==2||this.aircraft.kind==4)
            hitcount += 1;
        if(hitcount>200){
            tile.destroy();
            tile.position.x = 0;
            tile.position.y = 0;
            hitcount =0;
        }
        if(hitcount!=0)
            console.log(hitcount);
    },

    createShadow: function (object) {
        object.craftShadow = game.add.sprite(110, 345, 'aircraft'+object.kind);
        object.craftShadow.scale.setTo(0.3, 0.3);
        object.craftShadow.tint = 0x000000;
        object.craftShadow.alpha = 0.6;
        object.craftShadow.angle = 52;
    },

    updateShadow: function (object) {
        object.craftShadow.position.x = object.position.x + 40;
        object.craftShadow.position.y = object.position.y + 40;
    },

    createTextBoard: function () {
        this.scoreText = game.add.text(650, 420, 'Your Score:');
        this.scoreText.scale.setTo(0.8, 0.8);
        this.scoreNum = game.add.text(770, 480, Math.round(game.global.recent_score));
        this.scoreNum.scale.setTo(0.8, 0.8);
        this.scoreNum.anchor.set(1, 1);
    }, 

    updateTextBoard: function () {
        this.scoreNum.setText(Math.round(game.global.recent_score));
    },

    objectControl: function () {
        // console.log(this.craftID, this.Crafts.length);
        this.aircraft = this.Crafts[this.craftID];
        this.aircraft.inputEnabled = true;
        this.aircraft.input.enableDrag();
    },

    jump: function(){
        if (this.player.visible == false) {
            this.player.animations.play('fly1');
            this.player_shadow.animations.play('fly2');
            this.player.position.x = this.aircraft.position.x;
            this.player.position.y = this.aircraft.position.y-40;
            this.player.visible = true;
            this.player.body.velocity.x = 45*8;
            this.player.body.velocity.y = -31.4*8;

            this.player_shadow.position.x = this.aircraft.position.x;
            this.player_shadow.position.y = this.aircraft.position.y+40;
            this.player_shadow.visible = true;
            this.player_shadow.body.velocity.x = 45*8;
            this.player_shadow.body.velocity.y = -31.4*8;
            this.jumpTime = game.time.now;
        }
    },
    endjump: function(){
        this.player.body.velocity.x = 0;
        this.player.body.velocity.y = 0;
        this.player.visible = false;

        this.player_shadow.body.velocity.x = 0;
        this.player_shadow.body.velocity.y = 0;
        this.player_shadow.visible = false;
        this.aboardTime = game.time.now;
    },

    createItem: function () {
        if (game.time.now > this.itemTime + 4200 && game.time.now > this.lastTime + 1000) {
            this.itemTime = game.time.now;
            this.createOneItem();
            // console.log('shit');
        }
    },

    createOneItem: function () {
        var item;
        var x = Math.random()*360+540;
        var num = game.rnd.integerInRange(0,1);

        if(num==0){
            item = game.add.sprite(x, -114, 'heart');
            item.scale.setTo(0.2, 0.2);
            this.itemkind.push(0);
        }
        else if(num==1){
            item = game.add.sprite(x, -114, 'star');
            item.scale.setTo(0.75, 0.75);
            this.itemkind.push(1);
        }
        
        item.anchor.setTo(0.5, 0.5);
        this.item.push(item);
    },

    updateItem: function () {
        for(var i=0; i<this.item.length; i++)
        {
            var tile =this.item[i];
            
                
            tile.position.x-=2.25;
            tile.position.y+=1.57;
            //game.physics.arcade.collide(this.aircraft, tile, mainState.gameover(), null, this);
            // tile.angle = 52;

            
    
            // console.log(status);
            // tile.sendToBack();
            console.log(this.itemkind[i]);
            console.log(game.time.now-this.hitTime);
            if(Phaser.Math.distance(this.aircraft.position.x, this.aircraft.position.y , tile.position.x , tile.position.y)< 90) {
                if(this.itemkind[i] == 0 ) {
                    this.life =true;
                }
                    

                else if(this.itemkind[i] == 1)
                {
                    game.add.tween(this.aircraft).to({alpha: 0}, 700).yoyo(true).repeat(3).start();
                    this.hit = false;
                    this.hitTime = game.time.now;
                }
                tile.visible = false;
            }
                
            if(tile.position.x<-2600)
            {
                tile.destroy();
                this.item.splice(i, 1);
                this.itemkind.splice(i, 1);
            }
        }
            
        
    },

    jumpInterval: function () {
        if (this.aircraft.kind == 3) {
            return 800;
        }
        else return 600;
    },

    acceptArea: function () {
        if (this.aircraft.kind == 5) {
            return 141.4;
        }
        else return 100;
    },
    

    gameover:function(){
        status = 'gameover';
    },

    restart:function(){
        status = 'running';
        game.state.start('menu');
    }
};