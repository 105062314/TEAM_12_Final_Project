var explainState ={
    preload : function() {
        game.load.image('angel', './assets/gallifrey.jpg');
    },
    create : function(){
        this.keyboard = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        game.add.tileSprite(0, 0, 2000, 2000, 'angel').scale.setTo(1,1);

        var style = {fill: 'black', fontSize: '20px'};
        var title_style = {fill: 'white', fontSize: '40px'};
        
        game.add.text(game.width/2, 30, 'How To Play', title_style).anchor.setTo(0.5, 0.5);
        game.add.text(game.width/2, 100, 'Use mouse to drag your aircraft', style).anchor.setTo(0.5, 0.5);
        game.add.text(game.width/2, 140, 'Space to jump to other Spaceship', style).anchor.setTo(0.5, 0.5);
        game.add.text(game.width/2, 180, 'Press P to pause', style).anchor.setTo(0.5, 0.5);
        game.add.text(game.width/2, 220, 'Space B to end the game', style).anchor.setTo(0.5, 0.5);
        
        this.back = game.add.text(game.width/2, 450, 'press Space to back to Menu',  {fill: 'white', fontSize: '20px'});
        this.back.anchor.setTo(0.5, 0.5);
        
        game.add.tween(this.back).to({alpha : 0}, 1200).yoyo(true).loop().start();

        this.keyboard.onDown.add(function () {
            game.state.start('menu');
        }, this);

    },
    update : function(){}
}