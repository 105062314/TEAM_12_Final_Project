var firstime = true;
var menuState = {
    preload:function(){},
    create:function(){
        // this.cursor = game.input.keyboard.createCursorKeys();
        game.add.tileSprite(0, 0, 2000,2000, 'menu').scale.setTo(0.5,0.5);
        game.global.recent_score = undefined;
        //menu text
        var style = {fill: 'white', fontSize: '20px'};
        var title_style = {fill: 'white', fontSize: '40px'};
        this.NS = game.add.text(game.width/2, 80, 'Battle Ship', title_style).anchor.setTo(0.5, 0.5);
        this.MENU = game.add.text(game.width/2, 150, 'MENU', title_style).anchor.setTo(0.5, 0.5);
        this.index = game.add.text(250, 250, 'PLAY', style);//or 270
        this.index2 = game.add.text(380, 250, 'RANK', style);//or 430
        this.index3 = game.add.text(510, 250, 'INTRO', style);
        this.tri = game.add.sprite(225, 253 , 'tri'); //or245   index-20 //225  355  485
        this.tri.anchor.set(0.5, 0.5);
        this.tri.scale.setTo(0.15,0.15);
        this.tri.visible = true;

        //player
        game.physics.startSystem(Phaser.Physics.ARCADE);
        // this.aircraft = game.add.sprite(265, 315, 'aircraft0');
        // this.player = game.add.sprite(300, 300, 'player');
        
        // game.physics.arcade.enable(this.player);
        // game.physics.arcade.enable(this.aircraft);
        //this.player.facingLeft = false;

        //animations
        this.point = game.input.addPointer();
        this.point1 = game.input.addPointer();
        //floor
        this.menu_floor = game.add.sprite(0, 500, 'menu_floor');
        this.menu_floor.anchor.set(0,0);
        game.physics.arcade.enable(this.menu_floor); 
        this.menu_floor.body.immovable = true;
        //cyberman
        this.cyberman = game.add.sprite(400, 400, 'cyberman');
        this.cyberman.scale.setTo(2.5, 2.5);
        this.cyberman.anchor.set(0.5,0.5);
        game.physics.arcade.enable(this.cyberman);
        this.cyberman.facingRight = true;
        this.cyberman.body.gravity.y = 100;
        this.cyberman.animations.add('walk', [0,1,2,3], 4, true);
        this.cyberman.animations.add('rightwalk', [8,9,10,11], 4, true);
        this.cyberman.animations.add('leftwalk', [4,5,6,7], 4, true);
        //bubble1
        this.bubble1 = game.add.sprite(570, 330, 'bubble1');
        game.physics.arcade.enable(this.bubble1);
        this.bubble1.anchor.set(0.5, 0.5);
        this.bubble1.scale.setTo(0.5, 0.5);
        this.bubble1.visible = true;
        firstime = true;
        //bubble2
        this.bubble2 = game.add.sprite(230, 330, 'bubble2');
        game.physics.arcade.enable(this.bubble2);
        this.bubble2.anchor.set(0.5, 0.5);
        this.bubble2.scale.setTo(0.5, 0.5);
        this.bubble2.visible = false;
        //bubble3
        this.bubble3 = game.add.sprite(520, 330, 'bubble3');
        game.physics.arcade.enable(this.bubble3);
        this.bubble3.anchor.set(0.5, 0.5);
        this.bubble3.scale.setTo(0.5, 0.5);
        this.bubble3.visible = false;

        this.bgm = game.add.audio('menuMusic');
        this.bgm.loop = true;
        this.bgm.volumn = 0.6;
        this.bgm.play();
        
    },
    update: function(){
        game.physics.arcade.enable(this.tri, Phaser.Physics.ARCADE);
        game.physics.arcade.collide(this.cyberman, this.menu_floor);
        this.cursor = game.input.keyboard.createCursorKeys();
        this.menu();
        this.movecyberman();
        console.log(this.cyberman.body.position.x);
    },

    menu: function(){
        var keyboard2 = game.input.keyboard.addKeys({'enter': Phaser.Keyboard.ENTER});


        if(this.cyberman.body.position.x < 300){
            this.tri.position.x = 225;
        }
        else if(this.cyberman.body.position.x >420){// || (game.input.mousePointer.x<350&&game.input.mousePointer.isDown)||(game.input.pointer1.x<350&&game.input.pointer1.isDown)) {
            this.tri.position.x = 485;
        }
        else{//|| (game.input.mousePointer.x>=350&&game.input.mousePointer.isDown)||(game.input.pointer1.x>=350&&game.input.pointer1.isDown)){
            this.tri.position.x = 355;
        }
        if(keyboard2.enter.isDown){// || game.input.mousePointer.isDown || game.input.pointer1.isDown){
            this.bgm.stop();
            if(this.tri.position.x == 225) {
                game.global.player_name = prompt("Please enter your name", "name");
                game.state.start('main');
            }
            else if(this.tri.position.x == 355) {
                game.state.start('rank');
            } 
            else if(this.tri.position.x == 485){
                game.state.start('explain');
            }
        }
    },
    movecyberman:function(){
        //bubble1
        if(this.cyberman.body.position.x > 420) this.bubble1.visible = true;
        else this.bubble1.visible = false;
        if(this.bubble1.visible){
            if(this.cursor.left.isDown) 
                this.bubble1.body.velocity.x = -150;
            else if(this.cursor.right.isDown) 
                this.bubble1.body.velocity.x = 150;
            else this.bubble1.body.velocity.x = 0;
        }
        else this.bubble1.body.velocity.x = 0;
        //bubble2
        if(this.cyberman.body.position.x <= 300){
            this.bubble2.visible = true;
            this.bubble2.body.immovable = true;
        }
        else{
            this.bubble2.visible = false;
            this.bubble2.body.immovable = false;
        }
        if(this.bubble2.visible == true){
            if(this.cursor.left.isDown)
                this.bubble2.body.velocity.x = -150;
            else if(this.cursor.right.isDown)
                this.bubble2.body.velocity.x = 150;
            else this.bubble2.body.velocity.x = 0;
        }
        else this.bubble2.body.velocity.x = 0;
        //bubble3
        if(this.cyberman.body.position.x > 320 && this.bubble3.body.position.x >400 && this.cyberman.body.position.y > 375&&this.cyberman.body.position.x < 420) this.bubble3.visible = true;
        else this.bubble3.visible = false;
        if(this.bubble3.visible){
            if(this.cursor.left.isDown) 
                this.bubble3.body.velocity.x = -150;
            else if(this.cursor.right.isDown) 
                this.bubble3.body.velocity.x = 150;
            else this.bubble3.body.velocity.x = 0;
        }
        else this.bubble3.body.velocity.x = 0;

        //cyberman
        if(this.cyberman.body.position.y < 300) this.cyberman.frame = 0;
        else if(this.cursor.left.isDown){
            firstime = false;
            this.cyberman.body.velocity.x = -150;
            this.cyberman.facingRight = false;
            this.cyberman.animations.play('leftwalk');
            // console.log(this.cyberman.body.position.x);
            // if(this.cyberman.body.position.x < -50) this.cyberman.body.position.x = 780;
        }
        else if(this.cursor.right.isDown){
            firstime = false;
            this.cyberman.body.velocity.x = 150;
            this.cyberman.facingRight = true;
            this.cyberman.animations.play('rightwalk');
            // console.log(this.cyberman.body.position.x);
            // if(this.cyberman.body.position.x > 780) this.cyberman.body.position.x = -50;
        }
        else {
            this.cyberman.animations.stop();
            this.cyberman.body.velocity.x = 0;
            this.cyberman.frame = 0;
        }
    }
};